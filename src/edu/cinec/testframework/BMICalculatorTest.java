package edu.cinec.testframework;

import static edu.cinec.testframework.KUnit2.*;

public class BMICalculatorTest {

    public static void main(String[] args) {
        BMICalculator bmiCalculator = new BMICalculator();
        bmiCalculator.setWeight(55);//kilogram
        bmiCalculator.setHeight(1.5);//meters

        double answer = bmiCalculator.calculateBMI();
        double expectedResult = 24.4;
        //check and compare answer and expected result
        checkDoubleEquals(expectedResult,answer);
        checkDoubleNotEquals(expectedResult,answer);

        bmiCalculator.setAge(45); //set age
        int answerAge = bmiCalculator.getAge();
        int expectedResultAge = 45;
        //check and compare answer and expected result
        checkIntEquals(answerAge,expectedResultAge);

        bmiCalculator.setNama("Janaka"); //set name
        String answerNama = bmiCalculator.getNama();
        String expectedResultNama = "Janaka";
        //check and compare answer and expected result
        checkStringEquals(answerNama,expectedResultNama);
        report();



    }
}
