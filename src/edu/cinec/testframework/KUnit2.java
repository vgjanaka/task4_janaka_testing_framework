package edu.cinec.testframework;

import java.util.LinkedList;
import java.util.List;

public class KUnit2 {

    private static List<String> checks;
    private static int checksMade = 0;
    private static int passedChecks = 0;
    private static int failedChecks = 0;


    public static void checkDoubleEquals(double value1, double value2) {
        if (value1 == value2) {
            System.out.println("checkDoubleEquals Pass");
            addToReport(String.format("  %f == %f", value1, value2));
            passedChecks++;
            System.out.println("");
        } else {
            System.out.println("checkDoubleEquals Fail");
            addToReport(String.format("* %f == %f", value1, value2));
            failedChecks++;
        }
    }

    public static void checkDoubleNotEquals(double value1, double value2) {
        if (value1 != value2) {
            System.out.println("checkDoubleNotEquals Pass");
            addToReport(String.format("  %f != %f", value1, value2));
            passedChecks++;
        } else {
            System.out.println("checkDoubleNotEquals Fail");
            addToReport(String.format("* %f != %f", value1, value2));
            failedChecks++;
        }
    }

    public static void checkStringEquals(String value1, String value2) {
        if (value1.equals(value2)) {
            System.out.println("checkStringEquals Pass");
            addToReport(String.format("  %s == %s", value1, value2));
            passedChecks++;
        } else {
            System.out.println("checkStringEquals Fail");
            addToReport(String.format("* %s == %s", value1, value2));
            failedChecks++;
        }
    }

    public static void checkStringNotEquals(String value1, String value2) {
        if (!value1.equals(value2)) {
            System.out.println("checkStringNotEquals Pass");
            addToReport(String.format("  %s != %s", value1, value2));
            passedChecks++;
        } else {
            System.out.println("checkStringNotEquals Fail");
            addToReport(String.format("* %s != %s", value1, value2));
            failedChecks++;
        }
    }

    public static void checkIntEquals(int value1, int value2) {
        if (value1 == value2) {
            System.out.println("checkIntEquals Pass");
            addToReport(String.format("  %d == %d", value1, value2));
            passedChecks++;
        } else {
            System.out.println("checkIntEquals Fail");
            addToReport(String.format("* %d == %d", value1, value2));
            failedChecks++;
        }
    }

    public static void checkIntNotEquals(int value1, int value2) {
        if (value1 != value2) {
            System.out.println("checkIntNotEquals Pass");
            addToReport(String.format("  %d != %d", value1, value2));
            passedChecks++;
        } else {
            System.out.println("checkIntNotEquals Fail");
            addToReport(String.format("* %d != %d", value1, value2));
            failedChecks++;
        }
    }

    private static void addToReport(String txt) {
        if (checks == null) {
            checks = new LinkedList<>();
        }
        checks.add(String.format("%04d: %s", checksMade++, txt));
    }


    public static void report() {
        System.out.printf("%d checks passed\n", passedChecks);
        System.out.printf("%d checks failed\n", failedChecks);
        System.out.println();

        for (String check : checks) {
            System.out.println(check);
        }
    }

}
