package edu.cinec.testframework;

public class BMICalculator {

    private String nama;
    private int age;
    public double height;
    private double weight;

    public double calculateBMI() {
        double BMI = weight / (height * height);
        return BMI;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
